# lowcode-platform

#### 介绍
低代码搭建平台，技术栈：Vue3+TypeScript

#### 软件架构
软件架构说明


#### 安装教程
1.  yarn install
2.  npm run serve

#### 主要功能点
1. 拖拽组件到容器区
2. 画布组件选中态，拖拽位置及改变宽高
3. 标识线贴边检测
4. 撤销重做功能
5. 导入导出功能
6. 置顶、置底、删除、清空功能
7. 功能命令快捷键，支持单选多选
8. 画布中组件右键展示操作项
9. 画布中组件添加属性、绑定字段
10. 根据组件标识，通过作用域插槽，自定义组件行为

#### 目录结构
├─public
└─src
    ├─assets                             静态资源
    │  ├─images
    │  └─styles
    ├─data                               mock数据
    ├─packages
    |    ├─components                     组件列表
    │    |  ├─block-resize                   - 拖拽改变尺寸组件
    │    │  ├─dropdown                       - 右键下拉菜单组件
    │    │  ├─import-export-dialog           - 数据导入导出弹窗组件
    │    │  ├─number-range                   - 数字范围组件
    │    │  ├─table-prop-editor              - 属性添加组件
    │    │  └─visual-editor-block            - 画布中展示的组件
    │    ├─plugins
    │    │  └─command                        - 注册头部操作功能
    │    |     ├─Command.plugin.ts           - 命令注册基础函数
    │    |     └─VisualCommand.ts            - 头部功能命令注册
    │    ├─utils                             - 工具函数
    |    |   └─event.ts                      - 发布订阅工具函数
    |    ├─layout                             - 工具函数
    |    |  ├─visual-editor-content          - 画布容器
    │    │  ├─visual-editor-header           - 头部操作区容器
    │    │  ├─visual-editor-menu             - 左侧组件列表容器
    │    │  └─visual-editor-props            - 右侧属性管理容器
    |    └─Index.tsx                         - 容器包裹层
    ├─main.ts                                - 入口
    └─App.vue
    
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
