import { createApp } from 'vue';
import '@/assets/styles/index.scss';
import App from './App.vue';
// element-plus
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

const app = createApp(App)
app.use(ElementPlus);
app.mount('#app')
