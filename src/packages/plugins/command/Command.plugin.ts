// 命令注册 基类
import { reactive, onUnmounted } from 'vue';
import { KeyboardCode } from '../../utils/keyboard-code';
import { CommandExecute, Command } from './VisualCommand.d';

// 创建快捷键
export function useCommander() {
    const state = reactive({
        current: -1, // 游标
        queue: [] as CommandExecute[], // [{undo, redo}, {deleteUndo, deleteRedo}, {dragUndo, dragRedo} ...]
        commands: {} as Record<string, (...args: any[]) => void>, // {undo:()=>{redo()}, redo:()=>{redo()}, delete:()=> {redo()}, drag:()=> {redo()}, ...}
        commandArray: [] as Command[], // 注册的完整对象集合 [{name: 'xx', keywords: "", followQueue: false, execute: () => {redo, undo}}]
        destroyList: [] as ((() => void | undefined))[], // 移除事件 removeEventListener
    })
    // 注册函数
    const register = (command: Command) => {
        // 保存注册的对象
        state.commandArray.push(command);
        // 根据名称构建注册对象，并保存，等到调用的时候执行内部代码
        state.commands[command.name] = (...args) => {
            // 执行redo
            const { undo, redo } = command.execute(...args);
            redo();
            // 如果需要保存到queue中，则进行保存，游标加1
            if (command.followQueue) {
                let { queue, current } = state;
                if (queue.length > 0) {
                    queue = queue.slice(0, current + 1); // 1 => 2 => 3 => 4 => 3 => 5， 4不会保留
                    state.queue = queue;
                }
                queue.push({ undo, redo })
                state.current = current + 1;
            }
        }
        // 调用注册对象的init方法
        command.init && command.init();
    }

    // 键盘事件
    const keyboardEvent = (() => {
        // 键盘事件处理
        const onKeydown = (e: KeyboardEvent) => {
            // console.log(e);
            // 当前聚焦状态的Element不是document.body，则阻止
            if (document.activeElement !== document.body) return;
            const { keyCode, shiftKey, altKey, ctrlKey, metaKey } = e;
            // 复合键处理
            let keyString: string[] = [];
            if (ctrlKey || metaKey) keyString.push('ctrl');
            if (shiftKey) keyString.push('shift');
            if (altKey) keyString.push('alt');
            keyString.push(KeyboardCode[keyCode]);
            const keyNames = keyString.join('+');
            // console.log('当前键盘', keyNames);

            // 找到注册的方法并执行
            state.commandArray.forEach(({ keyboard, name }) => {
                if (!keyboard) return;
                const keys = Array.isArray(keyboard) ? keyboard : [keyboard];
                if (keys.indexOf(keyNames) > -1) {
                    state.commands[name](); // 执行
                    e.stopPropagation();
                    e.preventDefault(); // 阻止默认行为，比如Ctrl+D会触发收藏，现在阻止了，只执行我们定义的
                }
            })

        }
        const init = () => {
            // 订阅键盘按下事件
            window.addEventListener('keydown', onKeydown);
            // 销毁键盘事件
            return () => window.removeEventListener('keydown', onKeydown);
        }
        return init;
    })();

    // 注册撤销和重做
    const registerRedoUndo = () => {
        // 注册撤销
        register({
            name: 'undo',
            keyboard: 'ctrl+z',
            followQueue: false,
            execute: (...args) => {
                return {
                    undo: () => { /* console.log('undo撤销') */ },
                    redo: () => {
                        console.log('执行撤销');
                        if (state.current === -1) return;
                        const { undo } = state.queue[state.current];
                        console.log('执行撤销函数', undo);
                        !!undo && undo();
                        state.current -= 1;
                    }
                }
            }
        })
        // 注册重做
        register({
            name: 'redo',
            keyboard: ['ctrl+y', 'ctrl+shift+z'],
            followQueue: false,
            execute: (...args) => {
                return {
                    undo: () => { /* console.log('redo撤销') */ },
                    redo: () => {
                        console.log('执行重做');
                        const queueItem = state.queue[state.current + 1];
                        if (!!queueItem) {
                            queueItem.redo();
                            state.current++;
                        }
                    }
                }
            }
        })
    }

    // 初始化
    const init = () => {
        // 保存销毁函数
        state.commandArray.forEach(command => !!command.init && state.destroyList.push(command.init()));
        // 初始化键盘事件，添加移除事件
        state.destroyList.push(keyboardEvent());
        // 注册撤销和重做
        registerRedoUndo();
    }

    onUnmounted(() => {
        state.destroyList.forEach(fn => !!fn && fn());
    })

    return {
        state,
        register,
        init
    }
}