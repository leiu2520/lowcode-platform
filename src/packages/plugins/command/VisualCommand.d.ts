// 执行方法
export interface CommandExecute {
    undo: () => void,
    redo: () => void
}
// 定义操作名称、快捷键、执行方法
export interface Command {
    name: string; // 命令名称
    keyboard?: string | string[]; // 快捷键
    execute: (...args: any[]) => CommandExecute; // 撤销、反撤销时执行的函数
    followQueue?: boolean; // 是否需要在命令执行完毕后，将执行的undo，redo方法存放到命令队列中
    init?: () => any; // 注册命令是就会执行的方法
    data?: any; // 保存的数据，通常为redo，undo需要的数据
}