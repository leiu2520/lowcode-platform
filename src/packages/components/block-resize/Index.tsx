import { defineComponent, PropType } from 'vue';
import './Index.scss';
// 类型声明
import { VisualEditorBlockData } from '../visual-editor-block/Index.d';
import { VisualEditorComponent } from '../../layout/visual-editor-menu/Index.d';

// provide
import { VisualDragProvider } from '../../Index.utils';

enum Direction {
    start = 'start',
    center = "center",
    end = "end"
}


export const BlockResize = defineComponent({
    props: {
        block: {
            type: Object as PropType<VisualEditorBlockData>,
            required: true
        },
        component: {
            type: Object as PropType<VisualEditorComponent>,
            required: true
        }
    },
    setup(props) {
        return () => {
            const { width, height } = props.component.resize || {};

            const { dragStart, dragEnd } = VisualDragProvider.inject();

            const onMousedown = (() => {
                let data = {
                    startX: 0,
                    startY: 0,
                    startWidth: 0,
                    startHeight: 0,
                    startLeft: 0,
                    startTop: 0,
                    direction: {
                        horizontal: Direction.start,
                        vertical: Direction.start
                    },
                    dragging: false, // 是否正在拖拽
                }

                const mousedown = (e: MouseEvent, direction: { horizontal: Direction, vertical: Direction }) => {
                    e.stopPropagation(); // 拖拽的时候也调了mousedown方法（会导致一边改变大小，一边拖拽）
                    data = {
                        startX: e.clientX,
                        startY: e.clientY,
                        startWidth: props.block.width,
                        startHeight: props.block.height,
                        startLeft: props.block.left,
                        startTop: props.block.top,
                        direction,
                        dragging: false
                    }
                    document.addEventListener('mousemove', mousemove);
                    document.addEventListener('mouseup', mouseup);
                }

                const mousemove = (e: MouseEvent) => {
                    let { startX, startY, startWidth, startHeight, startLeft, startTop, direction, dragging } = data;
                    let { clientX: moveX, clientY: moveY } = e;

                    // 修改拖拽状态
                    if (!dragging) {
                        data.dragging = true;
                        dragStart.emit();
                    }

                    // 纵向拖拽-拖拽的是上、下的中间，则不允许向左右扩展宽度
                    if (direction.horizontal === Direction.center) {
                        moveX = startX
                    }
                    // 横向拖拽-拖拽的是左、右的中间，则不允许向上下扩展宽度
                    if (direction.vertical === Direction.center) {
                        moveY = startY
                    }

                    let durX = moveX - startX;
                    let durY = moveY - startY;
                    const block = props.block as VisualEditorBlockData;
                    // 如果拖拽的是左上角，则不需要改变尺寸
                    if (direction.vertical === Direction.start) {
                        durY = -durY;
                        block.top = startTop - durY;
                    }
                    if (direction.horizontal === Direction.start) {
                        durX = -durX;
                        block.left = startLeft - durX;
                    }

                    const width = startWidth + durX;
                    const height = startHeight + durY;
                    block.width = width;
                    block.height = height;
                    block.hasResize = true;

                }

                const mouseup = () => {
                    document.removeEventListener('mousemove', mousemove);
                    document.removeEventListener('mouseup', mouseup);
                    // 修改拖拽状态
                    if (data.dragging) {
                        data.dragging = false;
                        dragEnd.emit();
                    }
                }

                return mousedown;
            })()

            return <>
                {!!height && <>
                    <div class="block-resize block-resize-top"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.center,
                            vertical: Direction.start
                        })}>
                    </div>
                    <div class="block-resize block-resize-bottom"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.center,
                            vertical: Direction.end
                        })}>
                    </div>
                </>}
                {!!width && <>
                    <div class="block-resize block-resize-left"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.start,
                            vertical: Direction.center
                        })}>
                    </div>
                    <div class="block-resize block-resize-right"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.end,
                            vertical: Direction.center
                        })}>
                    </div>
                </>}
                {!!width && !!height && <>
                    <div class="block-resize block-resize-top-left"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.start,
                            vertical: Direction.start
                        })}>
                    </div>
                    <div class="block-resize block-resize-top-right"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.end,
                            vertical: Direction.start
                        })}>
                    </div>
                    <div class="block-resize block-resize-bottom-left"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.start,
                            vertical: Direction.end
                        })}>
                    </div>
                    <div class="block-resize block-resize-bottom-right"
                        onMousedown={e => onMousedown(e, {
                            horizontal: Direction.end,
                            vertical: Direction.end
                        })}>
                    </div>
                </>}
            </>
        }
    }

})