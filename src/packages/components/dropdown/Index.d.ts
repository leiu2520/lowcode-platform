// 下拉菜单选项
export interface IDropdownOption {
    evt: MouseEvent | HTMLElement;
    render: () => JSX.Element; // 内容，返回一个虚拟dom
}

// provide
export interface DropdownEvent {
    onClick: () => void
}