import { VisualEditorPropsType } from './Index.utils';

// 功能菜单的类型
export type VisualEditorProps = {
    type: VisualEditorPropsType; // 枚举 input、color、select、table
    label: string;
} & {
    options?: VisualEditorSelectOptions // 下拉框选项
} & {
    table?: VisualEditorTableOption // 表格数据
}

// 创建select下拉框属性
export type VisualEditorSelectOptions = {
    label: string,
    val: string
}[]

// 表格table
export type VisualEditorTableOption = {
    options: {
        label: string, // 列表显示文本
        field: string, // 列表绑定字段
    }[],
    showKey: string
}