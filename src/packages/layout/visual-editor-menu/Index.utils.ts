// 引入组件配置类型声明
import { VisualEditorComponent } from './Index.d';
// 引入属性类型声明
import { VisualEditorProps } from '../visual-editor-props/Index.d';

// 创建可视化编辑器配置
// 返回组件列表、组件映射、注册函数
export function createVisualEditorConfig() {
    const componentList: VisualEditorComponent[] = [];
    const componentMap: Record<string, VisualEditorComponent> = {};
    return {
        componentList,
        componentMap,
        registry: <Props extends Record<string, VisualEditorProps> = {}, Model extends Record<string, string> = {}>(key: string, componentOptions: {
            label: string,
            preview: () => JSX.Element,
            render: (data: {
                props: { [k in keyof Props]: any },
                model: Partial<{ [k in keyof Model]: any }>,
                size: { width?: number, height?: number },
                custom: Record<string, any>
            }) => JSX.Element,
            props?: Props,
            model?: Model,
            resize?: { width?: boolean, height?: boolean }
        }) => {
            let comp = { ...componentOptions, key };
            componentList.push(comp);
            componentMap[key] = comp;
        }
    }
}